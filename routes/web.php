<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MessageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::post('/message/{message}', [MessageController::class, 'restoreMessage'])->name('message.restore');

Route::resource('message', MessageController::class)->except(['update', 'show', 'create']);

Route::get('/', function () {
    return redirect("/message");
});

Route::fallback(function(){
    return redirect()->route('message.index');
});