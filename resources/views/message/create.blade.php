<h1 class="text-center text-4xl mb-2 font-bold">Erstellen</h1>
<form method="post" action="{{ route('message.store') }}">
    @csrf
    <label class="relative bg-white z-1 border-4 border-black p-2 top-6 -left-2" for="message">Neue Nachricht</label>
    <textarea name="message" id="message" rows="4" class="outline w-full p-4 pt-10 mb-2 rounded"></textarea>
    <div class="flex justify-center">
        <input class="border-4 border-black hover:bg-black hover:text-white px-1 rounded-lg cursor-pointer" type="submit" value="Erstellen">
    </div>
    @error('message')
        <div class="text-red-400 text-center mt-2">
            {{ $message }}    
        </div>
    @enderror
</form>
