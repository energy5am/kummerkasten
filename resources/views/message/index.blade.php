@extends('template')

@section('content')
    @include('message.create')
    <hr class="border-gray-300 my-6" />
    <h1 class="text-center text-4xl mb-4 font-bold">Nachrichten</h1>
    @forelse ($messages as $item)
        <div class="relative border-4 border-black rounded-lg p-4 mb-8">
            {{ $item->message }} 
            <span class=" bg-white border-4 border-black absolute rounded-full z-1 px-2 -bottom-4 -right-4 text-sm">
                {{ $item->created_at->diffForHumans() }}
            </span>
            <form method="post" action="{{ route('message.destroy', [$item]) }}">
                @csrf
                @method('DELETE')
                <input type="submit" value="&times;" class=" bg-white border-4 border-black absolute rounded-full px-2 -top-4 z-1 -right-4 hover:bg-black hover:text-white cursor-pointer">
            </form>
        </div>
    @empty 
        <div class="text-center mb-4">
            Keine Nachrichten. 
        </div>
    @endforelse
    <div x-data="{ trashed: false }">
        <div class="flex justify-center">
            <button @click="trashed=!trashed" class="border-4 border-black hover:bg-black hover:text-white px-2 rounded-lg cursor-pointer">
                <span x-text="trashed? 'Verstecke gelöschte Nachrichten.': 'Zeige gelöschte Nachrichten.'"></span>
            </button>    
        </div>
        <div x-show="trashed">
            <hr class="border-gray-300 my-6" />
            <h1 class="text-center text-4xl mb-4 font-bold text-gray-400">Gelöschte Nachrichten</h1>
            @forelse ($trashed as $item)
            <div class="relative border-4 border-gray-300 text-gray-400 rounded-lg p-4 mb-8 pr-10">
                    {{ $item->message }}
                    <span class="z-1 bg-white border-4 border-gray-300 text-gray-400 absolute rounded-full px-2 -bottom-4 -right-4 text-sm">
                        {{ $item->deleted_at->diffForHumans() }}
                    </span>    
                    <form method="post" action="{{ route('message.restore', [$item->id]) }}">
                        @csrf
                        <input type="submit" value="&#x21bb;" class="z-1 bg-white border-4 text-gray-400 border-gray-300 absolute rounded-full px-2 -top-4 -right-4 hover:bg-gray-300 hover:text-white cursor-pointer">
                    </form>
                </div>
            @empty
            <div class="text-center mb-6 text-gray-400">
                Keine gelöschten Nachrichten. 
            </div>
            @endforelse
        </div>
    </div>
@endsection