<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        return view('message.index', [
            'messages' => Message::latest()->get(), 
            'trashed' => Message::withTrashed()->latest('deleted_at')->where('deleted_at', '!=', 'null')->get()]);
    }

    public function restoreMessage($id)
    {
        Message::withTrashed()->find($id)->restore();
        return redirect()->route('message.index');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'message' => 'required'
        ]);
        Message::create($data);
        return redirect()->route('message.index');
    }

    public function destroy(Message $message)
    {
        $message->delete();
        return redirect()->route('message.index');
    }
}
